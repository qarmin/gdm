### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update
apt install -y git
git clone https://gitlab.gnome.org/GNOME/gdm.git
cd gdm

#Z#
apt build-dep -y gdm3
apt install -y meson ninja-build

meson build 

ninja -C build

```

### QTCreator Includes
```

```
